/**
 * calculates the elapsed time.
 */
public class MyTimer {

    Long previousTime;
    Long currentTime;

    public MyTimer() {
        previousTime = System.currentTimeMillis();
        currentTime = 0L;
    }

    /**
     * Calculates the new time since it started counting.
     */
    public void update() {
        currentTime = System.currentTimeMillis() - previousTime;
    }

    /**
     * retrieve the current elapsed time.
     *
     * @return A Long since we are dealing with Milliseconds.
     */
    public Long getCurrentTime() {
        return currentTime;
    }

    /**
     * Resets the elapsed time.
     */
    public void rest() {
        previousTime = System.currentTimeMillis();
    }
}
