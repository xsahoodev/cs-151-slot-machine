import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

//Misc. comment to test git repo push

public class SlotMachineDemo
{
        private static SlotMachine sm;
        private static SlotMachineView smv;
        private static SlotMachineController smc;
        private static JFrame myWindow;
        private static JDialog chooseSkin;

	public static void main(String[] args) 
        {
            myWindow = new JFrame("Slot Machine");
            myWindow.setSize(1200, 700);
            myWindow.setLocation(100, 50);
            myWindow.setLayout(new BoxLayout(myWindow.getContentPane(), BoxLayout.Y_AXIS));
            myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
            chooseSkin = new JDialog(myWindow);
            chooseSkin.setSize(700,200);
            chooseSkin.setLocation(150, 100);
            chooseSkin.setLayout(new GridLayout(2,1));
            JPanel panel1 = new JPanel();
            panel1.add(new JLabel("First, please enter the number of wheels as an integer (invalid input will result in the creation of 3 wheels):"));
            JTextField textField = new JTextField(5);
            panel1.add(textField);
            
            chooseSkin.add(panel1);
            JPanel panel2 = new JPanel();
            panel2.add(new JLabel("Then select a theme:"));
            JButton defaultSkin = new JButton("Default Theme");
            JButton skin2 = new JButton("Pokemon Theme");
            panel2.add(defaultSkin);
            panel2.add(skin2);
            chooseSkin.add(panel2);
            defaultSkin.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
                try {
                    Setting.numberOfSlots = Integer.parseInt(textField.getText());
                }
                catch (NumberFormatException error){
                    Setting.numberOfSlots = 3;
                }
                initGUI("res/Skin1.txt");
            }
        });
            skin2.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
                try {
                    Setting.numberOfSlots = Integer.parseInt(textField.getText());
                }
                catch (NumberFormatException error){
                    Setting.numberOfSlots = 3;
                }
                initGUI("res/Skin2.txt");
            }
        });
            chooseSkin.setVisible(true);
	}

        private static void initGUI(String filepath)
        {
            sm = new SlotMachine();
            
            smv = new SlotMachineView(sm, filepath);
            sm.addActionListener(smv);

            smc = new SlotMachineController();
            smc.addActionListener(sm);

            myWindow.add(smv);
            myWindow.add(smc);
            myWindow.setVisible(true);
            chooseSkin.setVisible(false);
        }
}