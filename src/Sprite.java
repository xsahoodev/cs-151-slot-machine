import java.awt.Image;

/**
 * holds information of image location and Image.
 *
 * @author Tech
 */
class Sprite {

    private int x;
    private int y;
    private Image image;

    public Sprite(Image image, int x, int y) {
        this.x = x;
        this.y = y;
        this.image = image;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getImage() {
        return image;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setImate(Image image) {
        this.image = image;
    }
}
