import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class SlotMachineView extends JComponent implements ActionListener {

    private ArrayList<Sprite> symbols = new ArrayList<Sprite>();
    private ArrayList<Wheel> wheels = new ArrayList<Wheel>();
    private JPanel gameWheels;
    private Image backGround;
    private Image graphicWin;
    private Image graphicLose;
    private Wheel wheel;
    private int tokens = Setting.numberOfPlays;
    private int previousTokens = tokens;
    private int wins = 0;
    private int previousWins = wins;
    private int tokenWon = 0;
    private int[] results;

    private ArrayList<Thread> threads = new ArrayList<Thread>();

    public SlotMachineView(SlotMachine sm, String skinFilePath) {
        this.results = sm.getResult();
        // load skin
        loadSkin(skinFilePath);
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 200));
        gameWheels = new JPanel(new FlowLayout());
        gameWheels.setOpaque(false);
        gameWheels.setVisible(true);
        this.add(gameWheels);
        // default wheels
        updateNumWheels(results.length);
        updateThreads(results.length);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SlotMachine s = (SlotMachine) e.getSource();
        int numOfWheels = s.getNumberOfSlots();

        int stopSymbols[] = new int[numOfWheels];
        stopSymbols = s.getResult();

        for (int i = 0; i < numOfWheels; i++) {
            wheels.get(i).setStopSymbol(stopSymbols[i]);
            wheels.get(i).setSpinHold(i * 1000);
            wheels.get(i).spin();
        }

        previousTokens = tokens;
        tokens = s.getTokenFiled();
        previousWins = wins;
        wins = s.getWin();

        tokenWon = s.getTokenFiledWon();

    }

    /**
     * creates wheels
     *
     * @param numOfWheels
     */
    public void updateNumWheels(int numOfWheels) {
        // first remove wheels incase its more then what we need
        int currentNumOfWheels = wheels.size();
        for (int i = 0; i < currentNumOfWheels; i++) {
            gameWheels.remove(wheels.get(i));
        }
        wheels.clear();

        // create wheels
        for (int i = 0; i < numOfWheels; i++) {
            Wheel temp = new Wheel(symbols,
                    symbols.get(0).getImage().getWidth(null),
                    symbols.get(0).getImage().getHeight(null));
            temp.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createRaisedBevelBorder(),
                    BorderFactory.createLoweredBevelBorder()));
            temp.setVisible(true);
            wheels.add(temp);
        }

        // add wheels to JPanel
        for (int i = 0; i < numOfWheels; i++) {
            gameWheels.add(wheels.get(i), BorderLayout.CENTER);
        }
    }

    /**
     * creates the threads for each wheel
     *
     * @param size
     */
    private void updateThreads(int size) {
        // create the threads
        for (int i = 0; i < size; i++) {
            threads.add(new Thread(wheels.get(i)));
            System.out.println(threads.size());
        }

        // start the threads
        for (int i = 0; i < size; i++) {
            threads.get(i).start();
        }
    }

    /**
     * Reads the image file
     *
     * @param file
     * @return
     */
    public BufferedImage getImage(String file) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(file));
        } catch (IOException e) {
            System.out.println(e);
        }
        return image;
    }

    /**
     * simple file reader must have the order of currently +background 1
     * +symbols 12
     *
     * @param fileName
     */
    public void loadSkin(String fileName) {
        try {
            File file = new File(fileName);
            BufferedReader in = new BufferedReader(new FileReader(file));
            Scanner s = new Scanner(in);
            // find background
            backGround = getImage(s.nextLine());

            // all thats left is to get the Symbols
            while (s.hasNext()) {
                symbols.add(new Sprite(getImage(s.nextLine()), 0, 0));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(backGround, 0, 0, null);
        g2.setColor(Color.red);
        Font font = new Font("System", Font.BOLD, 60);
        g2.setFont(font);

        int index = wheels.size() - 1;
        if (allStoped()) {
            g2.drawString("Tokens: " + Integer.toString(tokens), 0, 100);

            if (previousTokens < tokens) //slotmachine win
            {
                g2.drawString("You Win " + tokenWon + " token field!", this.getWidth() - 700, 100);
            }

            if (previousTokens > tokens)//slotmachine loss
            {
                g2.drawString("You Lose!", this.getWidth() - 350, 100);
            }
        } else {
            g2.drawString("Tokens: " + Integer.toString(previousTokens), 0, 100);
            g2.drawString("", this.getWidth() - 350, 100);
        }

        // only repaint a region, otherwise we get glitchy wheels
        repaint(0, 0, this.getWidth(), 200);
    }

    /**
     * checks if all the wheels are stopped.
     *
     * @return
     */
    public boolean allStoped() {
        for (int i = 0; i < wheels.size(); i++) {
            if (!wheels.get(i).getIsStoped()) {
                return false;
            }
        }

        return true;
    }
}
