import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.ImageIcon;

import javax.swing.JButton;
import javax.swing.JPanel;

public class SlotMachineController extends JPanel
{
	private JButton spinButton;
	private int skinSelected;
	private ArrayList<ActionListener> listeners;
	
	public SlotMachineController()
	{
                ImageIcon normalIcon = new ImageIcon("res/SpinNormal.png");
                ImageIcon pressedIcon = new ImageIcon("res/SpinPressed.png");

		spinButton = new JButton();
                spinButton.setIcon(normalIcon);
                spinButton.addMouseListener(new MouseAdapter()
                {
                    public void mousePressed(MouseEvent e) 
                    {
                        spinButton.setIcon(pressedIcon);
                    }

                    public void mouseReleased(MouseEvent e) 
                    {
                         spinButton.setIcon(normalIcon);
                    }
                });
                
		spinButton.addActionListener(new SpinListener());
		this.add(spinButton);
		
		
		listeners = new ArrayList<ActionListener>();
	}

	public void addActionListener(ActionListener a)
	{
		listeners.add(a);
	}
	
	class SpinListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			SlotMachine sm = new SlotMachine();
			ActionEvent newEvent = new ActionEvent(sm, ActionEvent.ACTION_FIRST, "update");
			for ( ActionListener a : listeners ) 
				a.actionPerformed(newEvent);
                }
	}
}