import java.awt.Dimension;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.JPanel;

class Wheel extends JPanel implements Runnable {

    private ArrayList<Sprite> images = new ArrayList<Sprite>();
    private int height;
    private int width;
    private int camY;
    private int speed;
    private int stopSymbol = 0;
    private MyTimer timer;
    private MyTimer spinFastTimer;
    private int delay = 50;
    private int fastSpinTime;
    private int minSpinSpeed = 20;
    private boolean isStoped = true;
    private Graphics2D g;

    // spin states
    private enum State {

        Spin,
        Stop,
    }
    private State currentState;

    /**
     * Initializes the JPanel
     *
     * @param images An ArrayList of images.
     * @param width The width of the JPanel.
     * @param height The height of the JPanel.
     */
    public Wheel(ArrayList<Sprite> images, int width, int height) {
        // set the width and height of JPanel
        this.width = width;
        this.height = height;
        setPreferredSize(new Dimension(width, height));
        // copy array
        //this.images = images;
        for (Sprite s : images) {
            this.images.add(s);
        }
        // camera
        camY = 0;

        currentState = State.Stop;

        // Default symbol the wheel should stop at
        stopSymbol = 0;

        // timers
        timer = new MyTimer();
        spinFastTimer = new MyTimer();

    }

    @Override
    public void run() {
        while (true) {

            //Graphics g = (Graphics) this.getGraphics();
            if (this.getGraphics() != null) {
                g = (Graphics2D) this.getGraphics();

                switch (currentState) {
                    case Spin:
                        isStoped = false;
                        spinState(g);
                        break;
                    case Stop:
                        stopState(g);
                        isStoped = true;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * checks to see if the sprite is in the JPanel.
     *
     * @param y The y coordinate.
     * @param height The height of the JPanel.
     * @return return true if image is in the JPanel.
     */
    private boolean checkYBounds(int y, int height) {
        if (y + height < 0 || y > height) {
            return false;
        }
        return true;
    }

    /**
     * In this state we just draw the Symbol that we should be stopped at.
     *
     * @param g The JPanel Graphics
     */
    private void stopState(Graphics2D g) {
        if (images.get(stopSymbol).getImage() != null) {
            g.drawImage(images.get(stopSymbol).getImage(), 0, 0, null);
        }
    }

    /**
     * In this state we Draw the whole spin process. From spinning fast to
     * slowing down.
     *
     * @param g The JPanel Graphics
     */
    private void spinState(Graphics2D g) {

        // how many array of images I should draw over and over again.
        int lengthOfArrays = 1000;
        int lengthOfImages = images.size();

        // update timers
        timer.update();
        spinFastTimer.update();

        // amount of time the wheel should spin fast for
        if (spinFastTimer.getCurrentTime() < fastSpinTime) {
            if (timer.getCurrentTime() >= delay) {
                camY += speed;
                timer.rest();

            }

        }

        // amount of time the wheel should slow down for
        if (spinFastTimer.getCurrentTime() >= fastSpinTime
                && speed > minSpinSpeed) {
            if (timer.getCurrentTime() >= delay) {
                camY += speed--;
                timer.rest();

            }
        } else {
            if (timer.getCurrentTime() >= delay) {
                camY += speed;
                timer.rest();
            }
        }

        // draws the same array of images over and over again
        for (int arrayNum = 0; arrayNum < lengthOfArrays; arrayNum++) {
            for (int index = 0; index < lengthOfImages; index++) {

                // calculate Y coords, dont car for X coords
                int y = (arrayNum * lengthOfImages + index) * -height;
                if (checkYBounds(y + camY, height)) {
                    if (speed <= minSpinSpeed) {
                        if (index == stopSymbol) {
                            if (y + camY >= 0) {
                                speed = 0;
                                currentState = State.Stop;
                            }
                        }
                    }
                    g.drawImage(images.get(index).getImage(),
                            images.get(index).getX(),
                            y + camY, null);
                }

            }
        }
    }

    /**
     * Set what symbol the spin should stop at.
     *
     * @param stop
     */
    public void setStopSymbol(int stop) {
        stopSymbol = stop;
    }

    /**
     * length of time it should spin in full speed. If set too long Images may
     * run out.
     *
     * @param spinHold length of time the wheel should spin in milliseconds
     */
    public void setSpinHold(int spinHold) {
        fastSpinTime = spinHold;
    }

    public boolean getIsStoped() {
        return isStoped;
    }

    /**
     * spins the wheel
     */
    public void spin() {
        speed = height;
        currentState = State.Spin;
        spinFastTimer.rest();
    }
}
