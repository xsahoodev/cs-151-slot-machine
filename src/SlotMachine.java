import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

public class SlotMachine implements ActionListener
{
	private int numberOfSlots;
	private int numberOfSymbols;
	
	private int[] result;	
	private int tokenFiled;
	private int win;
        private int tokenFiledWon;
	
	private ArrayList<Slot> slots = new ArrayList<>();
	
	private ArrayList<ActionListener> listeners;
	
	public SlotMachine()
	{
		this.numberOfSlots = Setting.numberOfSlots;
		this.numberOfSymbols = Setting.numberOfSymbols;
		this.tokenFiled = Setting.numberOfPlays;
		this.win = 0;
		
		result = new int[numberOfSlots];
		for(int i = 0; i < numberOfSlots; i++)
			slots.add(new Slot());
		
		listeners = new ArrayList<>();
	}
	
	public void addActionListener(ActionListener a)
	{
		listeners.add(a);
	}	


        @Override
	public void actionPerformed(ActionEvent e)
	{
            if(tokenFiled > 0)
            {
		Plays();
		for( ActionListener a : listeners )
			a.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_FIRST, "update"));
            }
	}
       
// Play & Calculate the token field and wins
	public void Plays()
	{
		if(getTokenFiled() > 0)
		{
			setTokenFiled((getTokenFiled()-1));
			getResults();
			checkWin();
		}
	}
	
	public void getResults()
	{
		for(int i = 0; i < result.length; i++)
			result[i] = slots.get(i).getSymbols();
	}
	
	public void checkWin()
	{
		int max = 1;
		
		int index = 0;
		for(int i = 0; i < result.length-1; i++)
		{
			int count = 1;
			for(int j = i+1; j < result.length; j++)
			{
				if(result[i] == result[j])
					count++;
			}
			if(max < count)
			{
				max = count;
				index = result[i];
			}
		}
		if(max > 1)
                {
                    setTokenFiledWon(max * index);
                    this.setTokenFiled(getTokenFiled() + getTokenFiledWon());
                    if(index > 0)
                    {
                        this.setWin(win+1);
                    }
                }
	}
        
// the token field that won
        public void setTokenFiledWon(int tokenFiledWon)
        {
            this.tokenFiledWon = tokenFiledWon;
        }
        public int getTokenFiledWon()
        {
            return tokenFiledWon;
        }

// Getter and Setter
	public void setNumberOfSlots(int numberOfSlots) { this.numberOfSlots = numberOfSlots; }
	public void setNumberOfSymbols(int numberOfSymbols) { this.numberOfSymbols = numberOfSymbols; }
	public void setTokenFiled(int tokenFiled) { this.tokenFiled = tokenFiled; }
	public void setWin(int win){ this.win = win; }
	public int getNumberOfSlots(){ return numberOfSlots; }
	public int getNumberOfSymbols(){ return numberOfSymbols; }
	public int getWin(){ return win; }
	public int getTokenFiled(){ return tokenFiled; }
	public int[] getResult() { return this.result; }
        
// 
	class Slot
	{
		private int[] Symbols;
		private int position = 0;

		public Slot()
		{
			Symbols = new int[numberOfSymbols];
			for(int i = 0; i < Symbols.length; i++)
				Symbols[i] = i;
		}
		
		public void setPosition(int newPosition) { this.position = newPosition; }
		public int getSymbol() { return Symbols[getPosition()]; }
		public int getPosition() { return position; }
		
		public int getSymbols()
		{
			Random random = new Random();
			setPosition((getPosition() + random.nextInt(100)) % numberOfSymbols);
			return getPosition();
		}
	}
}